<?php

if(PHP_SAPI != 'cli') die('Only CLI mode is supported!');
define('MASTER', 'inurosen');

final class InuBot
{

    private $_host = 'irc.twitch.tv';
    private $_port = 6667;
    private $_name = 'inubot';
    private $_pass = 'oauth:5bf0m6qsvpvj1db1ghbd24ekssfuh3';
    /**
     * @var Database $_db
     */
    private $_db;

    private $_socket = false;

    private $_lastcmd = 0;

    function __construct()
    {
        $this->_start();
    }

    private function _start()
    {
        define('INUBOT', true);
        require('inubot.inc/Helper.php');
        require('inubot.inc/Channel.php');
        require('inubot.inc/Channels.php');
        require('inubot.inc/Command.php');
        require('inubot.inc/CmdEngine.php');
        require('inubot.inc/Timer.php');
        require('inubot.inc/Database.php');
        require('inubot.inc/IRCPacket.php');
        require('inubot.inc/IRCPacket_Sender.php');
        $this->_db = Database::instance();
        echo 'Loading...'.PHP_EOL;
        $this->_db->getChannels();
        echo 'Connecting...'.PHP_EOL;
        $this->_socket = fsockopen($this->_host, $this->_port, $errno, $errstr, 2);
        if($this->_socket)
        {
            CmdEngine::SetSocket($this->_socket);
            CmdEngine::Send('PASS '.$this->_pass, $this->_socket);
            CmdEngine::Send('NICK '.$this->_name, $this->_socket);
            CmdEngine::Send('CAP REQ :twitch.tv/membership', $this->_socket);
            $this->_main();
            echo 'Stopping...'.PHP_EOL;
        }
    }

    private function _main()
    {
        stream_set_blocking(STDIN, 0);
        stream_set_blocking($this->_socket, 0);
        while(!feof($this->_socket))
        {
            $buffer = trim(fgets($this->_socket, 1024));
            if(!empty($buffer))
            {
                echo "\033[32m[RCV]\033[0m ".$buffer.PHP_EOL;
                if(Helper::startsWith($buffer, 'PING'))
                {
                    CmdEngine::Send("PONG :".substr($buffer, 6));
                }
                elseif(Helper::startsWith($buffer, ':'))
                {
                    $packet = new IRCPacket($buffer);
                    switch($packet->type)
                    {
                        case IRCPacket::MOTDEND:
                            $this->_joinChannels();
                            break;
                        case IRCPacket::USERLIST:
                            Channels::ParseUsers($packet);
                            break;
                        case IRCPacket::MODE:
                            Channels::SetMode($packet);
                            break;
                        case IRCPacket::JOIN:
                            if($packet->sender->nickname == $this->_name)
                            {
//                                $this->_greetChannel($packet->target);
                                $this->_getModerators($packet->target);
                            }
                            else
                            {
                                Channels::ProcessJoin($packet);
                            }
                            break;
                        case IRCPacket::PART:
                            Channels::ProcessPart($packet);
                            break;
                        case IRCPacket::PRIVMSG:
                            Channels::TickLines($packet);
                            if($packet->target != $this->_name)
                            {
                                Channels::ModerateMessage($packet);
                                $this->_chatCommand($packet);
                            }
                            else
                            {
                                //Channels::SetModerators($packet);
                            }
                            break;
                        default:
                            break;
                    }
//                    print_r($packet);
                    unset($packet);
                }
                flush();
            }
            Channels::FireTimers();
            $stdin = fgets(STDIN);
            if(!empty($stdin))
            {
                $this->_consoleCommand($stdin);
            }

        }
    }

    private function _joinChannels()
    {
            CmdEngine::Send('JOIN '.implode(',', array_keys(Channels::Get())));
    }

    private function _greetChannel($chan)
    {
        $text = 'Hello there. I am great master Inurosen\'s pet chat guardian. He is just testing my abilities now. You can try to !pet me RalpherZ';
        CmdEngine::SendMessage($chan, $text);
    }

    private function _getModerators($chan)
    {
        CmdEngine::SendMessage($chan, '.mods');
    }

    /**
     * @param $packet IRCPacket
     */
    private function _chatCommand($packet)
    {
        if(time() - $this->_lastcmd >= 2)
        {
            if(Helper::startsWith($packet->message, '!pet'))
            {
                if($packet->sender->nickname == MASTER)
                {
                    $response = 'Woof! My hero! RalpherZ';
                }
                elseif($packet->sender->nickname == 'flutterer')
                {
                    $responses = array(
                        'So cheaty, %s! I will tell my master! RalpherZ',
                        'Your hands smell like lilac, %s.',
                        'Your hands are so soft, %s.',
                    );
                    $response = sprintf($responses[array_rand($responses)], ucfirst($packet->sender->nickname));
                }
                elseif($packet->sender->nickname == 'bluesteelwheels')
                {
                    $response = sprintf('My master told me to not play with blue steel balls, %s! Hands off!', ucfirst($packet->sender->nickname));
                }
                elseif($packet->sender->nickname == 'sneakystavros')
                {
                    $response = sprintf('Your stavros is too sneaky, %s!', ucfirst($packet->sender->nickname));
                }
                elseif($packet->sender->nickname == 'emilyelizabeth1990')
                {
                    $response = sprintf('Do you have a leash for me, %s? Woof!', ucfirst($packet->sender->nickname));
                }
                elseif($packet->sender->nickname == 'tylerg21')
                {
                    $response = sprintf('Do you know how to make sticks, %s?', ucfirst($packet->sender->nickname));
                }
                elseif($packet->sender->nickname == 'tibbzeh')
                {
                    $response = sprintf('Where is my donkey, %s?!', ucfirst($packet->sender->nickname));
                }
                elseif($packet->sender->nickname == 'muted')
                {
                    $response = sprintf('What? I don\'t hear you, %s!', ucfirst($packet->sender->nickname));
                }
                elseif($packet->sender->nickname == 'kasandara')
                {
                    $responses = array(
                        'I can\'t believe that my master gave you a sword, %s. Now you are same as me, dawg!',
                        'Y\'know, %s, you\'re not that bad... Don\'t get me wrong, I\'m not trying to be friends with you.',
                        'Ah, it\'s you again, %s... Okay, you can pet me.',
                        'No, %s, I don\'t know where I got this moon sugar from.',
                    );
                    $response = sprintf($responses[array_rand($responses)], ucfirst($packet->sender->nickname));
                }
                else
                {
                    $responses = array(
                        'I have ticks, %s! Woof!',
                        'Give me a bone, %s! Woof!',
                        'You love to pet some dogs, %s, aren\'t you?',
                        '%s, my belly, my belly...',
                        'Wanna see my new Tail Chi style, %s?'
                    );
                    $response = sprintf($responses[array_rand($responses)], ucfirst($packet->sender->nickname));
                }
                CmdEngine::SendMessage($packet->target, $response);
            }
            elseif(Helper::startsWith($packet->message, '!addcmd'))
            {
                CmdEngine::cmd_addcmd($packet);
            }
            elseif(Helper::startsWith($packet->message, '!editcmd'))
            {
                CmdEngine::cmd_editcmd($packet);
            }
            elseif(Helper::startsWith($packet->message, '!delcmd'))
            {
                CmdEngine::cmd_delcmd($packet);
            }
            elseif(Helper::startsWith($packet->message, '!permit'))
            {
                CmdEngine::cmd_permit($packet);
            }
            elseif(Helper::startsWith($packet->message, '!moderation'))
            {
                CmdEngine::cmd_moderation($packet);
            }
            elseif(Helper::startsWith($packet->message, '!reg'))
            {
                CmdEngine::cmd_reg($packet);
            }
            elseif(Helper::startsWith($packet->message, '!'))
            {
                CmdEngine::cmd_custom($packet);
            }
        }
    }

    private function _consoleCommand($cmd)
    {
        if(Helper::startsWith($cmd, 'stop'))
        {
            CmdEngine::Send('QUIT :My master is calling me.');
        }
        elseif(Helper::startsWith($cmd, 'part'))
        {
            $parts = explode(' ', $cmd);
            if(isset($parts[1]) && Helper::startsWith($parts[1], '#'))
            {
                CmdEngine::SendMessage($parts[1], 'My master ordered me to leave. Good bye!');
                CmdEngine::Send('PART '.$parts[1]);
            }
        }
        elseif(Helper::startsWith($cmd, 'memory'))
        {
            echo 'Used memory: '.memory_get_usage().' bytes'.PHP_EOL;
        }
        elseif(Helper::startsWith($cmd, 'dump channels'))
        {
            print_r(Channels::Get()).PHP_EOL;
        }
        elseif(Helper::startsWith($cmd, 'users'))
        {
            $cmd = trim($cmd);
            $parts = explode(' ', $cmd);
            if(isset($parts[1]) && Helper::startsWith($parts[1], '#'))
            {
                foreach(Channels::Get($parts[1])->users AS $u)
                {
                    echo $u->username, ':', $u->access, PHP_EOL;
                }
            }
        }
    }

}

new InuBot();
