<?php

if(!defined('INUBOT')) die();

final class Command
{
    const OWNER = 9;
    const MODERATOR = 7;
    const REGULAR = 4;
    const SUBSCRIBER = 3;
    const FOLLOWER = 2;
    const VIEWER = 1;

    public $id;
    public $channel;
    public $command;
    public $response;
    public $access;
    public $last;
}
