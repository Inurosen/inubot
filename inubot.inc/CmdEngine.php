<?php

if(!defined('INUBOT')) die();

final class CmdEngine
{
    private static $_lastcmd = 0;
    private static $_socket;

    public static function SetSocket(&$socket)
    {
        self::$_socket = $socket;
    }

    public static function Send($cmd)
    {
        self::_send($cmd, self::$_socket);
    }

    public static function SendMessage($channel, $msg, $force = false)
    {
        if((time() - self::$_lastcmd >= 2 AND !$force) || $force)
        {
            $channel = trim($channel);
            self::_send('PRIVMSG '.$channel.' :'.$msg, self::$_socket);
            self::$_lastcmd = time();
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function cmd_addcmd(&$packet)
    {
        if(Channels::UserAccess($packet->sender->nickname, $packet->target) >= Command::MODERATOR OR $packet->sender->nickname == MASTER)
        {
            $words = explode(' ', $packet->message);
            array_shift($words);
            if(Helper::startsWith($words[0], '!'))
            {
                $cmd = $words[0];
            }
            else
            {
                $cmd = '!'.$words[0];
            }
            array_shift($words);
            if(Helper::startsWith($words[0], '-l='))
            {
                $levels = explode('=', $words[0]);
                switch(strtolower($levels[1]))
                {
                    case 'o':
                    case 'owner':
                        $lvl = Command::OWNER;
                        $txt = 'owner';
                        break;
                    case 'm':
                    case 'mod':
                        $lvl = Command::MODERATOR;
                        $txt = 'moderator';
                        break;
                    case 's':
                    case 'sub':
                        $lvl = Command::SUBSCRIBER;
                        $txt = 'subscriver';
                        break;
                    case 'r':
                    case 'reg':
                        $lvl = Command::REGULAR;
                        $txt = 'regular';
                        break;
                    case 'f':
                    case 'follower':
                        $lvl = Command::FOLLOWER;
                        $txt = 'follower';
                        break;
                    default:
                        $lvl = Command::VIEWER;
                        $txt = 'all';
                        break;
                }
                array_shift($words);
            }
            else
            {
                $lvl = Command::VIEWER;
                $txt = 'all';
            }
            $response = implode(' ', $words);
            $new = new Command();
            $new->command = $cmd;
            $new->channel = Channels::Get($packet->target)->id;
            $new->access = $lvl;
            $new->response = $response;
            $new->last = time();
            if(Database::instance()->addCommand($new))
            {
                Channels::SetCommand($packet->target, $new);
                $response = sprintf('%s, command %s has been added! Access level: %s. Response: %s', ucfirst($packet->sender->nickname), $cmd, $txt, $response);
                CmdEngine::SendMessage($packet->target, $response);
            }
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function cmd_editcmd(&$packet)
    {
        if(Channels::UserAccess($packet->sender->nickname, $packet->target) >= Command::MODERATOR OR $packet->sender->nickname == MASTER)
        {
            $words = explode(' ', $packet->message);
            array_shift($words);
            if(Helper::startsWith($words[0], '!'))
            {
                $cmd = $words[0];
            }
            else
            {
                $cmd = '!'.$words[0];
            }
            array_shift($words);
            if(isset(Channels::Get($packet->target)->commands[$cmd]))
            {
                $new = Channels::Get($packet->target)->commands[$cmd];
                if(Helper::startsWith($words[0], '-l='))
                {
                    $levels = explode('=', $words[0]);
                    switch(strtolower($levels[1]))
                    {
                        case 'o':
                        case 'owner':
                            $lvl = Command::OWNER;
                            $txt = 'owner';
                            break;
                        case 'm':
                        case 'mod':
                            $lvl = Command::MODERATOR;
                            $txt = 'moderator';
                            break;
                        case 's':
                        case 'sub':
                            $lvl = Command::SUBSCRIBER;
                            $txt = 'subscriver';
                            break;
                        case 'r':
                        case 'reg':
                            $lvl = Command::REGULAR;
                            $txt = 'regular';
                            break;
                        case 'f':
                        case 'follower':
                            $lvl = Command::FOLLOWER;
                            $txt = 'follower';
                            break;
                        default:
                            $lvl = Command::VIEWER;
                            $txt = 'all';
                            break;
                    }
                    array_shift($words);
                }
                else
                {
                    switch($new->access)
                    {
                        case Command::OWNER:
                            $txt = 'owner';
                            break;
                        case Command::MODERATOR:
                            $txt = 'moderator';
                            break;
                        case Command::SUBSCRIBER:
                            $txt = 'subscriver';
                            break;
                        case Command::REGULAR:
                            $txt = 'regular';
                            break;
                        case Command::FOLLOWER:
                            $txt = 'follower';
                            break;
                        default:
                            $txt = 'all';
                            break;
                    }
                    $lvl = $new->access;
                }
                $response = implode(' ', $words);
                $new->command = $cmd;
                $new->channel = Channels::Get($packet->target)->id;
                $new->access = $lvl;
                $new->response = $response;
                $new->last = time();
                if(Database::instance()->updateCommand($new))
                {
                    Channels::SetCommand($packet->target, $new);
                    $response = sprintf('%s, command %s has been changed! Access level: %s. Response: %s', ucfirst($packet->sender->nickname), $cmd, $txt, $response);
                    CmdEngine::SendMessage($packet->target, $response);
                }
            }
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function cmd_delcmd($packet)
    {
        if(Channels::UserAccess($packet->sender->nickname, $packet->target) >= Command::MODERATOR OR $packet->sender->nickname == MASTER)
        {
            $words = explode(' ', $packet->message);
            array_shift($words);
            if(Helper::startsWith($words[0], '!'))
            {
                $cmd = $words[0];
            }
            else
            {
                $cmd = '!'.$words[0];
            }
            if(isset(Channels::Get($packet->target)->commands[$cmd]))
            {
                $cmd_id = Channels::Get($packet->target)->commands[$cmd]->id;
                if(Database::instance()->deleteCommand($cmd_id))
                {
                    Channels::DeleteCommand($packet->target, $cmd);
                    $response = sprintf('%s, command %s has been deleted!', ucfirst($packet->sender->nickname), $cmd);
                    CmdEngine::SendMessage($packet->target, $response);
                }
            }
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function cmd_permit($packet)
    {
        if(Channels::Get($packet->target)->moderation)
        {
            if(Channels::UserAccess($packet->sender->nickname, $packet->target) >= Command::MODERATOR OR $packet->sender->nickname == MASTER)
            {
                $words = explode(' ', $packet->message);
                array_shift($words);
                Channels::PermitUser($packet->target, $words[0]);
                $response = sprintf('%s is now allowed to post a link!', $words[0]);
                CmdEngine::SendMessage($packet->target, $response);
            }
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function cmd_moderation($packet)
    {
        if(Channels::UserAccess($packet->sender->nickname, $packet->target) >= Command::MODERATOR OR $packet->sender->nickname == MASTER)
        {
            $words = explode(' ', $packet->message);
            array_shift($words);
            $channel = Channels::Get($packet->target);
            if($words[0] == 1 || strtolower($words[0]) == 'on')
            {
                $channel->moderation = 1;
                $response = sprintf('%s, moderation has been enabled.', ucfirst($packet->sender->nickname));
            }
            elseif($words[0] == 0 || strtolower($words[0]) == 'off')
            {
                $channel->moderation = 0;
                $response = sprintf('%s, moderation has been disabled.', ucfirst($packet->sender->nickname));
            }
            Channels::Set($channel);
            Database::instance()->updateChannel($channel);
            CmdEngine::SendMessage($packet->target, $response);
        }

    }

    /**
     * @param $packet IRCPacket
     */
    public static function cmd_reg($packet)
    {
        if(Channels::UserAccess($packet->sender->nickname, $packet->target) >= Command::MODERATOR OR $packet->sender->nickname == MASTER)
        {
            $words = explode(' ', $packet->message);
            array_shift($words);
            $channel = Channels::Get($packet->target);
            if($words[0] == '+' || strtolower($words[0]) == 'add')
            {
                if(!array_key_exists($words[1], $channel->regulars))
                {
                    $channel->regulars[$words[1]] = 1;
                    Database::instance()->addRegular($channel->id, $words[1]);
                    $response = sprintf('%s is now in regulars list.', ucfirst($words[1]));
                }
                else
                {
                    $response = sprintf('%s is already in regulars list.', ucfirst($words[1]));
                }
            }
            elseif($words[0] == '-' || strtolower($words[0]) == 'del')
            {

                if(array_key_exists($words[1], $channel->regulars))
                {
                    unset($channel->regulars[$words[1]]);
                    Database::instance()->deleteRegular($channel->id, $words[1]);
                    $response = sprintf('%s has been removed from regulars list.', ucfirst($words[1]));
                }
                else
                {
                    $response = sprintf('%s is not in regulars list.', ucfirst($words[1]));
                }
            }
            elseif($words[0] == '=' || strtolower($words[0]) == 'list')
            {
                $response = sprintf('Channel regulars: %s', implode(', ', array_keys($channel->regulars)));
            }
            Channels::Set($channel);
            CmdEngine::SendMessage($packet->target, $response);
        }

    }

    /**
     * @param $packet IRCPacket
     */
    public static function cmd_custom($packet)
    {
        $words = explode(' ', $packet->message);
        $words[0] = strtolower($words[0]);
        if(isset(Channels::Get($packet->target)->commands[$words[0]]))
        {
            $command = Channels::Get($packet->target)->commands[$words[0]];
            $last = $command->last;
            $now = time();
            if($now - $last > 10 AND (Channels::UserAccess($packet->sender->nickname, $packet->target) >= $command->access OR $packet->sender->nickname == MASTER))
            {
                $response = $command->response;
                array_shift($words);
                if(strpos($response, '$(query)') !== false)
                {
                    $response = str_replace('$(query)', implode(' ', $words), $response);
                }
                if(strpos($response, '$(user)') !== false)
                {
                    $target = !empty($words) ? $words[0] : $packet->sender->nickname;
                    $response = str_replace('$(user)', $target, $response);
                }
                if(strpos($response, '$(self)') !== false)
                {
                    $response = str_replace('$(self)', $packet->sender->nickname, $response);
                }
                CmdEngine::SendMessage($packet->target, $response);
                $command->last = $now;
                Channels::SetCommand($packet->target, $command);;
            }
        }
    }

    private static function _send($cmd)
    {
        @fwrite(self::$_socket, $cmd.PHP_EOL, strlen($cmd)+1);
        if(Helper::startsWith($cmd, 'PASS'))
        {
            echo "\033[33m[SND]\033[0m PASS ******".PHP_EOL;
        }
        else
        {
            echo "\033[33m[SND]\033[0m ".$cmd.PHP_EOL;
        }
    }

}
