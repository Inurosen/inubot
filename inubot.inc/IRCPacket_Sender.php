<?php

if(!defined('INUBOT')) die();

final class IRCPacket_Sender
{
    public $nickname;
    public $username;
    public $hostname;

    function __construct($sender)
    {
        if(preg_match('/(\w+)!(\w+)@([0-9A-Za-z-_.]+)/', $sender, $matches))
        {
            $this->nickname = $matches[1];
            $this->username = $matches[2];
            $this->hostname = $matches[3];
        }
    }

    function __toString()
    {
        return $this->nickname.'!'.$this->username.'@'.$this->hostname;
    }

}
