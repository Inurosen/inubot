<?php

if(!defined('INUBOT')) die();

require('User.php');
final class Channel
{
    public $id;
    public $channel;
    public $name;
    public $timers = array();
    public $commands = array();
    public $users = array();
    public $lines = 0;
    public $moderation = 0;
    public $moderators = array();
    public $regulars = array();
    public $permitted = array();
}
