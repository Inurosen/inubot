<?php

if(!defined('INUBOT')) die();

final class Database
{
    private $_host = 'localhost';
    private $_user = '';
    private $_pass = '';
    private $_database = 'inubot';

    private $_link = null;
    private static $_instance = null;

    function __construct()
    {
        self::$_instance = $this;
    }

    public static function instance()
    {
        if(is_null(self::$_instance))
        {
            new Database();
        }
        return self::$_instance;
    }

    private function _connect()
    {
        $this->_link = mysqli_connect($this->_host, $this->_user, $this->_pass, $this->_database);
        if(!$this->_link)
        {
            return false;
        }
        mysqli_query($this->_link, 'SET NAMES utf8');
    }

    private function _disconnect()
    {
        if($this->_link)
        {
            mysqli_close($this->_link);
            $this->_link = null;
        }
    }

    public function getChannels()
    {
        try
        {
            $this->_connect();
            $sql = mysqli_query($this->_link, 'SELECT `id`, `channel`, `moderation` FROM `channels` WHERE `enabled`=1');
            if($sql === false)
            {
                echo 'Can\'t load channels.';
                die();
            }
            while($row = mysqli_fetch_array($sql, MYSQL_ASSOC))
            {
                $channel = new Channel();
                $channel->id = $row['id'];
                $channel->channel = '#'.$row['channel'];
                $channel->name = $row['channel'];
                $channel->timers = $this->getTimers($row['id']);
                $channel->commands = $this->getCommands($row['id']);
                $channel->lines = 0;
                $channel->moderation = $row['moderation'];
                $user = new User();
                $user->username = $channel->name;
                $user->access = Command::OWNER;
                $channel->users[$channel->name] = $user;
                $channel->permitted = array();
                $channel->regulars = $this->getRegulars($row['id']);
                Channels::Set($channel);
            }
            $this->_disconnect();
        }
        catch(Exception $e)
        {
        }
    }

    public function getTimers($channel_id)
    {
        try
        {
            $this->_connect();
            $sql = mysqli_query($this->_link, 'SELECT * FROM `timers` WHERE `channel_id`='.$channel_id);
            if($sql === false)
            {
                echo 'Can\'t load timers.';
                die();
            }
            $result = array();
            while($row = mysqli_fetch_array($sql, MYSQL_ASSOC))
            {
                $timer = new Timer();
                $timer->id = $row['id'];
                $timer->channel = $channel_id;
                $timer->name = $row['name'];
                $timer->interval = $row['interval'];
                $timer->lines = $row['lines'];
                $timer->message = $row['message'];
                $timer->last = time();
                $timer->lastline = 0;
                $result[$row['id']] = $timer;
            }
            $this->_disconnect();
            return $result;
        }
        catch(Exception $e)
        {

        }
    }

    public function getRegulars($channel_id)
    {
        try
        {
            $this->_connect();
            $sql = mysqli_query($this->_link, 'SELECT * FROM `regulars` WHERE `channel_id`='.$channel_id);
            if($sql === false)
            {
                echo 'Can\'t load regulars.';
                die();
            }
            $result = array();
            while($row = mysqli_fetch_array($sql, MYSQL_ASSOC))
            {
                $result[$row['username']] = 1;
            }
            $this->_disconnect();
            return $result;
        }
        catch(Exception $e)
        {

        }
    }

    public function getCommands($channel_id)
    {
        try
        {
            $this->_connect();
            $sql = mysqli_query($this->_link, 'SELECT * FROM `commands` WHERE `channel_id`='.$channel_id);
            if($sql === false)
            {
                echo 'Can\'t load commands.';
                die();
            }
            $result = array();
            while($row = mysqli_fetch_array($sql, MYSQL_ASSOC))
            {
                $command = new Command();
                $command->id = $row['id'];
                $command->channel = $channel_id;
                $command->command = $row['command'];
                $command->response = $row['response'];
                $command->access = $row['access'];
                $command->last = 0;
                $result[$row['command']] = $command;
            }
            $this->_disconnect();
            return $result;
        }
        catch(Exception $e)
        {

        }
    }

    public function updateTimerTick($timer_id)
    {
        try
        {
            $this->_connect();
            $stmt = mysqli_prepare($this->_link, 'UPDATE `timers` SET `last`=? WHERE `id`=?');
            $stmt->bind_param('ii', time(), $timer_id);
            if($stmt->execute())
            {
                $this->_disconnect();
                return true;
            }
        }
        catch(Exception $e)
        {

        }
    }

    /**
     * @param $command Command
     * @return bool
     */
    public function addCommand(Command $command)
    {
        try
        {
            $this->_connect();
            $stmt = mysqli_prepare($this->_link, 'INSERT INTO `commands` (`channel_id`, `command`, `response`, `access`, `enabled`) VALUES(?, ?, ?, ?, 1)');
            $stmt->bind_param('issi', $command->channel, $command->command, $command->response, $command->access);
            if($stmt->execute())
            {
                $this->_disconnect();
                return true;
            }
            else
            {
                $this->_disconnect();
                return false;
            }
        }
        catch(Exception $e)
        {

        }
    }

    /**
     * @param $channel_id int
     * @param $username string
     * @return bool
     */
    public function addRegular($channel_id, $username)
    {
        try
        {
            $this->_connect();
            $stmt = mysqli_prepare($this->_link, 'INSERT INTO `regulars` (`channel_id`, `username`) VALUES(?, ?)');
            $stmt->bind_param('is', $channel_id, $username);
            if($stmt->execute())
            {
                $this->_disconnect();
                return true;
            }
            else
            {
                $this->_disconnect();
                return false;
            }
        }
        catch(Exception $e)
        {

        }
    }

    /**
     * @param $channel_id int
     * @param $username string
     * @return bool
     */
    public function deleteRegular($channel_id, $username)
    {
        try
        {
            $this->_connect();
            $stmt = mysqli_prepare($this->_link, 'DELETE FROM `regulars` WHERE `channel_id` = ? AND `username` = ?');
            $stmt->bind_param('is', $channel_id, $username);
            if($stmt->execute())
            {
                $this->_disconnect();
                return true;
            }
            else
            {
                $this->_disconnect();
                return false;
            }
        }
        catch(Exception $e)
        {

        }
    }


    /**
     * @param $command Command
     * @return bool
     */
    public function updateCommand(Command $command)
    {
        try
        {
            $this->_connect();
            $stmt = mysqli_prepare($this->_link, 'UPDATE `commands` SET `response` = ?, `access`= ? WHERE `id` = ?');
            $stmt->bind_param('sii', $command->response, $command->access, $command->id);
            if($stmt->execute())
            {
                $this->_disconnect();
                return true;
            }
            else
            {
                $this->_disconnect();
                return false;
            }
        }
        catch(Exception $e)
        {

        }
    }

    /**
     * @param $channel Channel
     * @return bool
     */
    public function updateChannel(Channel $channel)
    {
        try
        {
            $this->_connect();
            $stmt = mysqli_prepare($this->_link, 'UPDATE `channels` SET `moderation` = ? WHERE `id` = ?');
            $stmt->bind_param('ii', $channel->moderation, $channel->id);
            if($stmt->execute())
            {
                $this->_disconnect();
                return true;
            }
            else
            {
                $this->_disconnect();
                return false;
            }
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }

    public function deleteCommand($command_id)
    {
        try
        {
            $this->_connect();
            $stmt = mysqli_prepare($this->_link, 'DELETE FROM `commands` WHERE `id` = ?');
            $stmt->bind_param('i', $command_id);
            if($stmt->execute())
            {
                $this->_disconnect();
                return true;
            }
            else
            {
                $this->_disconnect();
                return false;
            }
        }
        catch(Exception $e)
        {

        }
    }


}
