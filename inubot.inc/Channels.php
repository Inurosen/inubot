<?php

if(!defined('INUBOT')) die();

final class Channels
{
    private static $_channels = array();

    public static function Set($channel)
    {
        self::$_channels[$channel->channel] = $channel;
    }

    /**
     * @param null $channel
     * @return array|bool|Channel
     */
    public static function Get($channel = null)
    {
        if(is_null($channel)) return self::$_channels;
        if(array_key_exists($channel, self::$_channels))
        {
            return self::$_channels[$channel];
        }
        else
        {
            return false;
        }
    }

    public static function UserAccess($username, $channel)
    {
        if(isset(self::$_channels[$channel]->users[$username]))
        {
            return self::$_channels[$channel]->users[$username]->access;
        }
        else
        {
            return Command::VIEWER;
        }

    }

    /**
     * @param $channel
     * @param $command Command
     */
    public static function SetCommand($channel, $command)
    {
        self::$_channels[$channel]->commands[$command->command] = $command;
    }

    /**
     * @param $channel
     * @param $command
     */
    public static function DeleteCommand($channel, $command)
    {
        unset(self::$_channels[$channel]->commands[$command]);
    }

    /**
     * @param $channel
     * @param $user
     */
    public static function PermitUser($channel, $user)
    {
        $user = strtolower($user);
        self::$_channels[$channel]->permitted[$user] = true;
    }

    public static function FireTimers()
    {
        $now = time();
        foreach(self::$_channels as $chank => $chan)
        {
            foreach($chan->timers as $tk => $timer)
            {
                if(($now - $timer->last)/60 > $timer->interval AND ($chan->lines - $timer->lastline) > $timer->lines)
                {
                    if(CmdEngine::SendMessage($chan->channel, $timer->message))
                    {
                        self::$_channels[$chank]->timers[$tk]->last = $now;
                        self::$_channels[$chank]->timers[$tk]->lastline = $chan->lines;
                        Database::instance()->updateTimerTick($tk);
                    }
                }
            }
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function TickLines($packet)
    {
        if(array_key_exists($packet->target, self::$_channels))
        {
            self::$_channels[$packet->target]->lines += 1;
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function ParseUsers($packet)
    {
        $special = explode('= ', $packet->special);
        $channel = $special[1];
        if(array_key_exists($channel, self::$_channels))
        {
            $_users = explode(' ', $packet->message);
            foreach($_users as $user)
            {
                self::$_channels[$channel]->users[$user] = new User();
                self::$_channels[$channel]->users[$user]->username = $user;
                if($user == self::$_channels[$channel]->name)
                {
                    self::$_channels[$channel]->users[$user]->access = Command::OWNER;
                }
                elseif(array_key_exists($user, self::$_channels[$channel]->regulars))
                {
                    self::$_channels[$channel]->users[$user]->access = Command::REGULAR;
                }
                else
                {
                    self::$_channels[$channel]->users[$user]->access = Command::VIEWER;
                }
            }
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function ProcessJoin($packet)
    {
        if($packet->sender->username != self::$_channels[$packet->target]->name)
        {
            if(array_key_exists($packet->target, self::$_channels))
            {
                self::$_channels[$packet->target]->users[$packet->sender->username] = new User();
                self::$_channels[$packet->target]->users[$packet->sender->username]->username = $packet->sender->nickname;
                if(array_key_exists($packet->sender->username, self::$_channels[$packet->target]->regulars))
                {
                    self::$_channels[$packet->target]->users[$packet->sender->username]->access = Command::REGULAR;
                }
                else
                {
                    self::$_channels[$packet->target]->users[$packet->sender->username]->access = Command::VIEWER;
                }
            }
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function ProcessPart($packet)
    {
        if($packet->sender->username != self::$_channels[$packet->target]->name)
        {
            if(array_key_exists($packet->target, self::$_channels))
            {
                if(isset(self::$_channels[$packet->target]->users[$packet->sender->username]))
                {
                    unset(self::$_channels[$packet->target]->users[$packet->sender->username]);
                    unset(self::$_channels[$packet->target]->permitted[$packet->sender->username]);
                }
            }
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function SetMode($packet)
    {
        $special = explode(' ', $packet->special);
        if(count($special) == 2 AND $special[1] != self::$_channels[$packet->target]->name)
        {
            if($special[0] == '+o')
            {
                self::$_channels[$packet->target]->users[$special[1]]->access = Command::MODERATOR;
                if(!in_array($special[1], self::$_channels[$packet->target]->moderators))
                {
                    self::$_channels[$packet->target]->moderators[] = $special[1];
                }
            }
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function ModerateMessage($packet)
    {
        if(self::$_channels[$packet->target]->moderation)
        {
            if(isset(self::$_channels[$packet->target]->users[$packet->sender->username]))
            {
                $user = self::$_channels[$packet->target]->users[$packet->sender->username];
            }
            else
            {
                $user = new User();
                $user->username = $packet->sender->username;
                $user->access = Command::VIEWER;
            }
            $permitted = array_key_exists($user->username, self::$_channels[$packet->target]->permitted);
            if($user->access < Command::REGULAR AND !in_array($user->username, self::$_channels[$packet->target]->moderators))
            {
                if(preg_match('@((https?|ftp)://|(www|ftp)\.)[a-z0-9-]+(\.[a-z0-9-]+)+([/?].*)?@', $packet->message)) //urls
                {
                    if(!$permitted)
                    {
                        $response = $packet->sender->username.', you should ask for permission before posting a link!';
                        CmdEngine::SendMessage($packet->target, '.timeout '.$packet->sender->username.' 1', true);
                        CmdEngine::SendMessage($packet->target, $response, true);
                    }
                    else
                    {
                        unset(self::$_channels[$packet->target]->permitted[$user->username]);
                    }
                }
                elseif(preg_match('/twitch\.tv\/\w+/', $packet->message)) //twitch links
                {
                    if(!$permitted)
                    {
                        $response = $packet->sender->username.', you should ask for permission before posting a link!';
                        CmdEngine::SendMessage($packet->target, '.timeout '.$packet->sender->username.' 1', true);
                        CmdEngine::SendMessage($packet->target, $response, true);
                    }
                    else
                    {
                        unset(self::$_channels[$packet->target]->permitted[$user->username]);
                    }
                }
                elseif(preg_match('/\p{Lu}{7,}/', $packet->message)) //caps
                {
                    echo $packet->message.PHP_EOL;
                    $response = $packet->sender->username.', no caps!';
                    CmdEngine::SendMessage($packet->target, '.timeout '.$packet->sender->username.' 1', true);
                    CmdEngine::SendMessage($packet->target, $response, true);
                }

            }
        }
    }

    /**
     * @param $packet IRCPacket
     */
    public static function SetModerators($packet)
    {
        if(Helper::startsWith($packet->message, 'The moderators of this room are: '))
        {
            $message = str_replace('The moderators of this room are: ', '', $packet->message);
            self::$_channels[$packet->target]->moderators = explode(', ', $message);
        }
    }

}
