<?php

if(!defined('INUBOT')) die();

final class IRCPacket
{
    /**
     * @var IRCPacket_Sender $sender
     */
    public $sender;
    public $target;
    public $type;
    public $message;
    public $special;

    const MOTDEND = '376';
    const USERLIST = '353';
    const JOIN = 'JOIN';
    const MODE = 'MODE';
    const PART = 'PART';
    const PRIVMSG = 'PRIVMSG';

    function __construct($packet)
    {
        $divide = explode(' :', $packet);
        $head = explode(' ', $divide[0]);
        $this->type = $head[1];
        switch($this->type)
        {
            case self::PRIVMSG:
                $this->sender = new IRCPacket_Sender($head[0]);
                $this->target = $head[2];
                break;
            case self::JOIN || self::PART:
                $this->sender = new IRCPacket_Sender($head[0]);
                $this->target = $head[2];
                break;
            default:
                $this->sender = new IRCPacket_Sender($head[0]);
                break;
        }
        array_shift($head);
        array_shift($head);
        array_shift($head);
        $this->special = implode(' ', $head);
        if(isset($divide[1]))
            $this->message = $divide[1];
    }

}
