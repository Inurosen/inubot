<?php

if(!defined('INUBOT')) die();

final class Timer
{
    public $id;
    public $channel;
    public $name;
    public $interval;
    public $lines;
    public $message;
    public $last;
    public $lastline;
}
